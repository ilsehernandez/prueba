﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp2.Classes.Visuals
{
    class MovementLimits
    {
        int top;
        int left;
        List<Obstacle> obstacles;
        List<Weapon> weapons;

        public MovementLimits(int top, int left, List<Obstacle> obstacles, List<Weapon> weapons)
        {
            this.top = top;
            this.left = left;
            this.obstacles = obstacles;
            this.weapons = weapons;
        }

        public int Top { get => top; set => top = value; }
        public int Left { get => left; set => left = value; }
        internal List<Obstacle> Obstacles { get => obstacles; set => obstacles = value; }
        internal List<Weapon> Weapons { get => weapons; set => weapons = value; }

    }
}
