﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp2.Classes.Items;

namespace WindowsFormsApp2.Classes.Characters
{
    class Enemy:ZObject
    {
        byte vida;


        public Enemy(Coordenadas coordenadas) : base(coordenadas)
        {
            vida = 100;
        }

        public byte Vida { get => vida; }

        public void MostrarInformacion()
        {

            Console.WriteLine("Soy un enemigo");

        }

        public void BajarVida(byte valor)
        {
            vida -= valor;
            //if vida == 0 morir
        }

    }
}
