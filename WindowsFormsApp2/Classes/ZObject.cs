﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp2.Classes.Visuals;

namespace WindowsFormsApp2.Classes
{
    class ZObject
    {
        Coordenadas coordenadas;

        public ZObject(Coordenadas coordenadas)
        {
            this.coordenadas = coordenadas;
        }

        internal Coordenadas Coordenadas { get => coordenadas; set => coordenadas = value; }
    }
}
