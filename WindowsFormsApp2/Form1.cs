﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            
            control.Size = new Size(control.Size.Width, control.Size.Height);
            
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
                pictureBox1.Top -= pictureBox1.Height;
            if (e.KeyCode == Keys.Down)
                pictureBox1.Top += pictureBox1.Height;
            if (e.KeyCode == Keys.Left)
                pictureBox1.Left -= pictureBox1.Width;
            if (e.KeyCode == Keys.Right)
                pictureBox1.Left += pictureBox1.Width;
        }
    }
}
