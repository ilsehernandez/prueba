﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp2.Classes.Characters
{
    class Hero
    {
        Weapon equipada;

        public Hero(Coordenadas coordenadas) : base(coordenadas)
        {
            equipada = null;
        }

        internal Weapon Equipada { get => equipada; set => equipada = value; }

        public void MostrarInformacion()
        {
            Console.Beep(400, 100);
        }

        public void Atacar(Enemy elEnemigo)
        {
            elEnemigo.BajarVida(Equipada.Damage);
        }

        public void equipar(Weapon arma)
        {
            equipada = arma;
        }

    }
}
