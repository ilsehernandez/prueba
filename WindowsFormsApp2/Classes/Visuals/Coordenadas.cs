﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp2.Classes.Visuals
{
    class Coordenadas
    {
        int top;
        int left;

        public Coordenadas(int top, int left)
        {
            this.top = top;
            this.left = left;
        }

        public int Top { get => top; set => top = value; }
        public int Left { get => left; set => left = value; }

    }
}
