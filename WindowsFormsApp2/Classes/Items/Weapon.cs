﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp2.Classes.Items
{
    class Weapon
    {
        byte damage;

        public Weapon(byte damage, Coordenadas coordenadas) : base(coordenadas)
        {
            this.damage = damage;
        }

        public byte Damage { get => damage; }

    }
}
